public class Animal {
    /**
     * @type String
     */
    private String name;
    /**
     * @type String
     */
    private String classis;

    /**
     * @type String
     */
    private String ordo;


    public Animal(String name, String classis, String ordo) {
        this.name = name;
        this.classis = classis;
        this.ordo = classis;
    }

    public Animal() {
        this.name = "Hydrochoerus hydrochaeris";
        this.classis = "Mammalia";
        this.ordo = "Rodentia";
    }

    public String getName() {
        return null;
    }

    public String getOrdo() {
        return null;
    }

    public String getClassis() {
        return null;
    }
}
