package serializer;

import serializer.classloaders.JarClassLoader;
import serializer.serializer.SerializeToXml;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;

public class Main {

    private static final int MAX_BUFFER_SIZE = 1024;
    private final static String jarFileUrl = "https://bitbucket.org/romanlex/classloader/raw/ecc3cc0354ea919291064b4b19c4ccfbfcf91084/myjar/Animal.jar";
    private static File downloadedFile;
    private static int readBytes = 0;

    public static void main(String[] args) {
        try {
            String className = "Animal";
            serializeClass(className);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found: " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    private static void serializeClass(String name) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        ClassLoader jar = new JarClassLoader();

        getJarFromUrl(jarFileUrl, name);

        if(downloadedFile == null || !Files.exists(Paths.get(downloadedFile.getCanonicalPath())))
            throw new ClassNotFoundException("Cannot load JAR with class from url.");

        Class serializedClass = jar.loadClass(name);

        if(serializedClass == null)
            throw new IOException("Cannot load class from JAR");

        Collection<Object> animals = new ArrayList<>();
        animals.add(serializedClass.newInstance());

        try {
            SerializeToXml.serialize(animals);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    private static boolean getJarFromUrl(String url, String fileName) throws IOException {
        URL uri = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) uri.openConnection();
        connection.setRequestMethod("GET");
        int status = connection.getResponseCode();
        long size = connection.getContentLength();

        if (status / 100 != 2)
        {
            throw new IOException("-> Ignoring url. Response code: " + Integer.toString(status));
        }

        if((connection.getContentType() != "text/plain") &&
                !url.endsWith(".jar") ) {
            throw new IOException("-> Content type of this url not text/plain. Change url to text file.");
        }

        try {
            BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
            downloadedFile = new File(fileName + ".jar");
            FileOutputStream fw = new FileOutputStream(downloadedFile);
            byte[] btBuffer = new byte[MAX_BUFFER_SIZE];
            int intRead = 0;
            while( (intRead = bis.read(btBuffer)) != -1 ){
                fw.write(btBuffer, 0, intRead);
                readBytes = readBytes + intRead;
            }
            fw.close();
            System.out.println("Download successfull. File is save to dir: " + downloadedFile.getAbsolutePath());
            return true;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }



}
