package serializer.serializer;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.beans.XMLEncoder;
import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;

/**
 * Created by roman on 10.02.17.
 */
public class SerializeToXml {
    private static final String SERIALIZED_FILE_NAME = "peoples.xml";
    static Document document;


    public static void serialize(Collection<Object> obj) throws ParserConfigurationException, IOException, SAXException, TransformerException, IllegalAccessException, InstantiationException {
        System.out.println("Serialize " + obj.getClass().getSimpleName() + " object...");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();
        Document doc = impl.createDocument(null,
                null,
                null);

        Element _el_root = doc.createElement("collection");

        for (Object _obj:
             obj) {
            Element _el_childs = doc.createElement("object");
            _el_childs.setAttribute("type", _obj.getClass().getSimpleName());

            /*
            Class[] interfaces = _obj.getClass().getInterfaces();
            for(Class cInterface : interfaces) {
                _el_childs.setAttribute("implements", cInterface.getSimpleName() );
            }
            */

            Field[] objectFields = _obj.getClass().getDeclaredFields();
            for (Field field:
                    objectFields) {
                field.setAccessible(true);
                Object value = field.get(_obj);
                Element _field = doc.createElement("field");
                _field.setAttribute("type", field.getType().getSimpleName());
                _field.setAttribute("value", value.toString());
                _field.setAttribute("id", field.getName());
                _el_childs.appendChild(_field);
            }

            Element _el_methods = doc.createElement("methods");
            Method[] methods = _obj.getClass().getMethods();
            for (Method method:
                    methods) {
                method.setAccessible(true);
                Element _el_method = doc.createElement("method");
                _el_method.setAttribute("name", method.getName());
                _el_method.setAttribute("return", method.getReturnType().getSimpleName());
                _el_method.setAttribute("parametersCount", Integer.toString(method.getParameterCount()));
                _el_methods.appendChild(_el_method);
            }

            _el_childs.appendChild(_el_methods);
            _el_root.appendChild(_el_childs);
        }

        doc.appendChild(_el_root);

        DOMSource dom = new DOMSource(doc);

        File file = new File(Object.class.getSimpleName() + ".xml");
        StreamResult result = new StreamResult(file);

        TransformerFactory transFactory = TransformerFactory.newInstance(); // Об этом подробней в 4 вопросе
        Transformer transformer = transFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        transformer.transform(dom, result);

    }

    public static void serializeByXMLEncoder(Object o) {
        XMLEncoder encoder = null;
        try {
            encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(SERIALIZED_FILE_NAME)));
        } catch (FileNotFoundException fileNotFound) {
            System.out.println("ERROR: While Creating or Opening the File");
        }
        encoder.writeObject(o);
        encoder.close();
    }
}
